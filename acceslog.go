package wtstat

import (
	"encoding/csv"
	"errors"
	"io"
)

type AccessLog struct {
	name     string
	upstream io.Reader
	reader   *csv.Reader
}

func (a *AccessLog) Read() (entry Entry, err error) {

	// Read next record from csv
	record, err := a.reader.Read()
	if err == io.EOF {
		err = errors.New("EOF")
		return
	}
	if err != nil {
		return
	}
	if len(record) != 45 {
		err = errors.New("bad line format")
		return
	}
	return record, nil
}

func (a *AccessLog) Name() string {
	return a.name
}

func NewAccesLog(name string, upstream io.Reader) (l *AccessLog, err error) {

	// create csv reader from upstream
	reader := csv.NewReader(upstream)
	reader.Comma = '|' // when records are separated by tab

	l = &AccessLog{
		name:     name,
		upstream: upstream,
		reader:   reader,
	}
	return

}
