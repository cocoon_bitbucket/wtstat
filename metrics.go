package wtstat

/*

	define prometheus metrics for sniffer

	sniffer subject :  <sniffer_name>


	main labels:



*/

import (

	//"fmt"
	//"time"

	//"runtime"

	"github.com/prometheus/client_golang/prometheus"
)

// InitMetrics : declare metrics
func InitMetrics() {

	prometheus.MustRegister(EventCounter)
	prometheus.MustRegister(QualifiedEventCounter)

	prometheus.MustRegister(Uptime)

	// register myorange metrics
	prometheus.MustRegister(EdenRequestCounted)
	prometheus.MustRegister(EdenRequestOk)
	prometheus.MustRegister(EdenOkAccountConsulted)

}

// EventCounter : generic event counter for wstt
// parse 0:OK 1:parsing error
// domain:  domain field of event
var EventCounter = prometheus.NewCounterVec(
	prometheus.CounterOpts{
		Name: "wtstat_count",
		Help: "number of wtstat generic events",
	},
	[]string{"parse", "domain"},
)

//
//	counter for qualified wtstat event
//
// domain : the domain field of event ( eg gameloft )
// tag: "" in case of multiple event, type of event for a domain
var QualifiedEventCounter = prometheus.NewCounterVec(
	prometheus.CounterOpts{
		Name: "wtstat_event",
		Help: "number of qualified events",
	},
	[]string{"domain", "tag"},
)

// Uptime : uptime in seconds of the source
var Uptime = prometheus.NewGaugeVec(
	prometheus.GaugeOpts{
		Name: "uptime",
		Help: "count of seconds since started",
	},
	[]string{"source", "name"}, // eg device=wtstat , name=1
)

//
// My orange metrics
//
var EdenRequestCounted = prometheus.NewCounterVec(
	prometheus.CounterOpts{
		Name: "eden_request_counted",
		Help: "",
	},
	[]string{"domain"},
)

var EdenRequestOk = prometheus.NewCounterVec(
	prometheus.CounterOpts{
		Name: "eden_request_ok",
		Help: "",
	},
	[]string{"domain"},
)

var EdenOkAccountConsulted = prometheus.NewCounterVec(
	prometheus.CounterOpts{
		Name: "eden_ok_account_consulted",
		Help: "",
	},
	[]string{"domain"},
)

// status of the sniffer
// 0:
var scannerStatus = prometheus.NewGaugeVec(
	prometheus.GaugeOpts{
		Name: "status",
		Help: "status of the sniffer",
	},
	[]string{"name"},
)

//
// metrics recorders ( helpers to record metrics)
//

// IncrementEventCounter : generic event counter
func IncrementEventCounter(parse, domain string) {
	EventCounter.With(
		prometheus.Labels{
			"parse": parse, "domain": domain,
		}).Inc()
}

func IncrementQualifiedEventCounter(domain, tag string) {
	QualifiedEventCounter.With(
		prometheus.Labels{
			"domain": domain, "tag": tag,
		}).Inc()
}

// RecordPacketCounter : number of packets treated by source
//func RecordPacketCounter(count float64, source string) {
//	PacketCounter.With(prometheus.Labels{"source": source}).Set(count)
//}

// RecordSnifferStatus  0: initialized , 1 running
func RecordSnifferStatus(status float64, name string) {
	scannerStatus.With(prometheus.Labels{"name": name}).Set(status)
}
