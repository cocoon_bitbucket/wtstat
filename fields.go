package wtstat

// defines field codes for access_log
type FieldType int

const (
	TimeStamp   FieldType = iota //[04/11/2019:01:13:01]
	GoRoCo                       //[G6R14C0]
	MCO                          //OSN
	_                            //wt
	_                            //0
	VS                           //VS=001DEFAULT_WTCOM
	TS                           //TS=001DEFAULT_TS_WTCOM
	ttg                          //1147496
	ttl                          //2
	HttpCode                     //200
	_                            //sau=-
	cai                          //cai=4
	nat                          //nat=1
	_                            //uit=-
	_                            //mss=-
	_                            //ENDINFO
	remoteIP                     //10.161.167.141
	clientIP                     //10.161.167.141
	Domain                       //gameloft.orange.sn
	_                            //-
	_                            //-
	_                            //-
	_                            //-
	_                            //1
	_                            //"012345678901234567890123"
	MSISDN                       //221784414055
	_                            //"-"
	_                            //0
	_                            //"Client-IP=10.161.167.141"
	_                            //"Client-IP=10.161.167.141"
	_                            //-
	_                            //-
	_                            //-
	_                            //-
	_                            //-
	ARCidCookie                  //697850
	combine                      //combined=100000000
	request                      //"GET /subscription_index/3452/?staticua=TRUE&opref=adwtelecoming-JNO3331572829980be6fseg5ja4h97hjrgo0|pub_id%3A1&click_id=JNO3331572829980be6fseg5ja4h97hjrgo0&pub_id=1&ms_sid=4&country=185&fromcrt=1&updateadid=31921126777&from=ADID-434303&sv=126h8hzvernjmi38z51nor27j&main= HTTP/1.1"
	Cookie                       //"FAKE-COOKIE"
	userAgent                    //"Mozilla/5.0 (Linux; Android 8.1.0; SM-A260F Build/OPR6; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 SamsungBrowser/7.2 Chrome/70.0.3538.110 Mobile Safari/537.36"
	_                            //45809
	_                            //2
	_                            //-
	_                            //"-"
	LAST                         //

)
