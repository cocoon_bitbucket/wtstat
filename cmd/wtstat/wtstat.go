package main

import (
	"bitbucket.com/cocoon_bitbucket/wtstat"
	"bitbucket.com/cocoon_bitbucket/wtstat/source"
	"flag"
	"fmt"
	"io"
	"log"
	"os"
)

var (
	Version = "0.1"

	fPort = flag.String("port", "8080", "network port for http endpoint")
	fPipe = flag.String("pipe", "", "wtstat linux pipe")
	fFile = flag.String("file", "", "wtstat log filename")

	replay = flag.String("replay", "", " '': respect timestamp, '0': no delay, '1s': one second delay ")
	debug  = flag.Bool("debug", false, "verbose")

	helper = `read from a linux pipe OR a wtstat log file and build promethehus metrics
at localhost:8080/metrics
sample usage : wtstat  --pipe /var/ines/pipe 
options:
--pipe string
    	linux pipe of wtstat lines
--file string
		log file to read
--replay string 
	'': respect source timestamp, '0': no delay, '1s': one second delay 
--debug
    	verbose mode
`
)

func run() {

	//var source datasource.DataSource
	var upstream io.Reader
	var err error

	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, fmt.Sprintf("wtstat %s\n", Version))
		fmt.Fprintf(os.Stderr, helper)
	}

	flag.Parse()

	// check --file or --pipe
	if *fPipe == "" && *fFile == "" {
		fmt.Print("ERROR: source not specified, set --pipe or --file option\n\n")
		fmt.Print(helper)
		os.Exit(-1)
	}
	if *fPipe != "" && *fFile != "" {
		fmt.Print("ERROR: set --pipe or --file option not both\n\n")
		fmt.Print(helper)
		os.Exit(-1)
	}

	// find  mode ( pipe or file )
	mode := "PIPE"
	if *fFile != "" {
		mode = "FILE"
	}

	config := &wtstat.ScannerConfig{
		Mode:       mode,
		Debug:      *debug,
		ReplayMode: *replay,
	}

	log.Printf("starting")
	switch mode {
	case "FILE":
		// read from a log file
		log.Printf("create a file reader with %s\n", *fFile)
		upstream, err = source.NewFileSource(*fFile)
		if err != nil {
			log.Printf("%s\n", err.Error())
			os.Exit(-1)
		}

	default:

		// no replay mode while reading from pipe
		if *replay != "" {
			log.Printf("Warning: --replay option is invalid for --pipe mode, replay is set to no-delay\n")
		}

		// read from a linux pipe
		log.Printf("create a pipereader with %s\n", *fPipe)
		upstream, err = source.NewPipeSource(*fPipe)
		if err != nil {
			log.Printf("%s\n", err.Error())
			os.Exit(-1)
		}
		config.ReplayMode = "0"
	}

	accesslog, err := wtstat.NewAccesLog("accesslog", upstream)
	if err != nil {
		log.Fatal(err)
	}

	config.CsvSource = accesslog

	// start metrics server
	wtstat.InitMetrics()
	go wtstat.Server(*fPort)

	// start scanner
	wtstat.Scanner(config)

	log.Printf("exiting\n")

}

func main() {
	run()
}
