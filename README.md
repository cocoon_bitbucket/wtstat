# WtStat


## Acceslog Parser for prometheus metrics


# usage

    wstat --pipe /tmp/wtstat.pipe --port 8080

then configure prometheus to scrap <host>:8080/metrics

2 metrics are available

## wtstat_count

    HELP wtstat_count number of wtstat generic events
    TYPE wtstat_count counter
    wtstat_count{domain="config.rcs.mnc001.mcc608.pub.3gppnetwork.org",parse=""} 39*


## wtstat_event

    HELP wtstat_event number of qualified events
    TYPE wtstat_event counter
    wtstat_event{domain="config.rcs.mnc001.mcc608.pub.3gppnetwork.org",tag=""} 39*


# features

## ultra fast
    parse 200 000 lines of access log per second

## easy to deploy
    just one binary, built-in prometheus server

## easy to maintain

* a single repository from parsing access log to publish to  prometheus
* only a small set og golang is needed to add filters ( == != contains regexp )
* all the power and speed of go to add more complex logic




# domains handled

* rcs    config.rcs.mnc001.mcc608.pub.3gppnetwork.org

* gameloft      gameloft.orange.sn

* myorange      myorange.orange.sn

* waaat           waaat.orange.sn

* omapposn        omapposn.orange.sn


# Build wtstat executable

prerequisites

* linux machine with build tools ( make )
* golang installed
* git
* access to internet


    cd /tmp
    git clone https://bitbucket.org/cocoon_bitbucket/wtstat.git
    cd wtstat
    make compile

find the executable for linux at

    build/packages/linux_amd64/wtsat



# run the demo ( with docker )

the demo consist of a wstat instance reading a sample access log file at the rate of 1 line/second

a prometheus, altermanager and grafana instances

prerequisites

* linux machine with build tools ( make )
* golang installed
* git
* access to internet
* docker and docker-compose installed


    cd /tmp
    git clone https://bitbucket.org/cocoon_bitbucket/wtstat.git
    cd wtstat
    make compile

    cd build/docker/demo
    docker-compose up

with firefox connect to prometheus ui and grafana

* raw wtstat metrics at <host>:8080
* prometheus at <host>:9090
* grafana at <host>:3000  admin/password
* alertmanager at <host>:9093




# HOW TO

## add a new metric named "sample" for domain "sample.org"


1) create the sample.go file from template sample.tmpl

    cp sample.tmpl sample.go

2) edit sample.go

replace all occurence of {{NAME} with "Sample" , The capital S at the begining is MANDATORY
replace {{DOMAIN} with your domain

* const {{NAME}}Domain = "{{DOMAIN}}"  ==>  const SampleDomain = "sample.org"
* func {{NAME}}Handler(line Entry) {  ==> func SampleHandler(line Entry) {

3) add your filter instructions

see an example at ./examples/sample.go

you can find all the field identifiers in file

    fields.go



4) edit dispatch.go to add the sample case in the switch instruction

    case SampleDomain:
    		SampleHandler(line)

see example at ./examples/dispatch.go


5) compile wtstat

    make compile


get the linux executable at ./build/packages/linux_amd64/wtstat


6) deploy wtstat and run it

    wtstat --pipe /tmp/wtstat.pipe


7) configure prometheus to scrap <host>:8080/metrics
