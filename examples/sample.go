package wtstat

import "strings"

const (
	SampleDomain = "sample.org"
)

func SampleHandler(line Entry) {

	domain := line.Field(Domain)
	tag := ""

	//
	// the filter instructions start below
	//

	// exclude line when field request does not contain "subscrition_index"
	if strings.Contains(line.Field(request), "subscription_index") == false {
		return
	}

	// exclude line when field Cookie contains "LKU-COOKIE
	if strings.Contains(line.Field(Cookie), "LKU-COOKIE") == true {
		return
	}
	// set tag="same" if clientIP == RemoteIP , otherwise set tag="diff"
	if line.Field(clientIP) == line.Field(remoteIP) {
		tag = "same"
	} else {
		tag = "diff"
	}

	//
	// create the prometheus metric
	//
	IncrementQualifiedEventCounter(domain, tag)

	return
}
