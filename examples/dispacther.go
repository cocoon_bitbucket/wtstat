package wtstat

func Dispatch(line *WtstatLog) {

	domain := line.Field(Domain)
	IncrementEventCounter("", domain)

	switch domain {

	case GameloftDomain:
		GameloftHandler(line)

	case SampleDomain:
		SampleHandler(line)
	}

}
