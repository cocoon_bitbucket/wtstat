module bitbucket.com/cocoon_bitbucket/wtstat

go 1.13

require (
	bitbucket.org/orangeparis/ines v0.0.0-20200228182721-a989d9fd381b
	bitbucket.org/orangeparis/snifferlog v0.0.0-20200306181116-ea5bc217792f
	github.com/golang/protobuf v1.3.4 // indirect
	github.com/influxdata/go-syslog/v3 v3.0.0 // indirect
	github.com/nats-io/nats-server/v2 v2.1.4 // indirect
	github.com/prometheus/client_golang v1.5.0
)
