package wtstat

import (
	"log"
	"net/http"

	"github.com/prometheus/client_golang/prometheus/promhttp"
)

func Server(port string) {
	addr := "0.0.0.0:" + port

	log.Printf("start prometheus endpoint at %s/metrics\n", addr)
	http.Handle("/metrics", promhttp.Handler())
	http.ListenAndServe(addr, nil)
}
