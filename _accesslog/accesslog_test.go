package _accesslog

import (
	"encoding/csv"
	"io"
	"log"
	"os"
	"testing"
)

func TestExploreAccessLog(t *testing.T) {

	filename := "../samples/access_log"

	// Open the file
	csvfile, err := os.Open(filename)
	if err != nil {
		log.Fatalln("Couldn't open the csv file", err)
	}

	// Parse the file
	r := csv.NewReader(csvfile)
	r.Comma = '|' // when records are separated by tab
	//r.Comment = "#" // ignores the line starting with '#'
	//r := csv.NewReader(bufio.NewReader(csvfile))

	// Iterate through the records
	for {
		// Read each record from csv
		record, err := r.Read()
		if err == io.EOF {
			break
		}
		if err != nil {
			log.Fatal(err)
		}
		if len(record) != 45 {
			log.Fatal("bad format")
		}

		//fmt.Printf("Question: %s Answer %s\n", record[0], record[1])
	}
}

func TestAcceslogFields(t *testing.T) {

	filename := "../samples/access_log"

	// Open the file
	csvfile, err := os.Open(filename)
	if err != nil {
		log.Fatalln("Couldn't open the csv file", err)
	}

	// Parse the file
	r := csv.NewReader(csvfile)
	r.Comma = '|' // when records are separated by tab

	// Iterate through the records
	for {
		// Read each record from csv
		record, err := r.Read()
		if err == io.EOF {
			break
		}
		if err != nil {
			log.Fatal(err)
		}
		if len(record) != 45 {
			log.Fatal("bad format")
		}

		//timestamp := record[TimeStamp]
		//domain := record[Domain]
		//request := record[request]
		cookie := record[Cookie]
		//ts := record[TS]

		//println(timestamp, domain, request, cookie, ts)
		//println(ts)
		println(cookie)

		//fmt.Printf("Question: %s Answer %s\n", record[0], record[1])
	}
}

func TestAcceslogObject(t *testing.T) {

	filename := "../samples/access_log"

	l, err := NewFileAccesLog(filename)
	if err != nil {
		log.Fatalf(err.Error())
	}

	// Iterate through the records
	for {
		// Read each record from csv
		entry, err := l.Read()
		if err == io.EOF {
			break
		}
		if err != nil {
			log.Fatal(err)
		}

		//timestamp := record[TimeStamp]
		//domain := record[Domain]
		//request := record[request]
		//cookie := entry.Field(Cookie)

		cookie := entry[Cookie]
		ts, _ := entry.Timestamp()
		println(ts.String())
		//ts := record[TS]

		//println(timestamp, domain, request, cookie, ts)
		//println(ts)
		println(cookie)

		//fmt.Printf("Question: %s Answer %s\n", record[0], record[1])
	}
}
