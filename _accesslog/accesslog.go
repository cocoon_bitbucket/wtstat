package _accesslog

import (
	"bitbucket.com/cocoon_bitbucket/wtstat"
	"encoding/csv"
	"errors"
	"io"
	"log"
	"os"
)

type FileAccessLog struct {
	Filename string
	reader   *csv.Reader
}

func (a *FileAccessLog) Read() (entry wtstat.Entry, err error) {

	// Read next record from csv
	record, err := a.reader.Read()
	if err == io.EOF {
		err = errors.New("EOF")
		return
	}
	if err != nil {
		return
	}
	if len(record) != 45 {
		err = errors.New("bad line format")
		return
	}
	return record, nil
}

func NewFileAccesLog(filename string) (l *FileAccessLog, err error) {

	// Open the file
	csvfile, err := os.Open(filename)
	if err != nil {
		log.Printf("Couldn't open the csv file: %s\n", err)
		return
	}

	// Parse the file
	reader := csv.NewReader(csvfile)
	reader.Comma = '|' // when records are separated by tab

	l = &FileAccessLog{
		Filename: filename,
		reader:   reader,
	}

	return

}
