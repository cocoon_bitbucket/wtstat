package wtstat

import "strings"

const (
	OtherDomain = "other.orange.sn"
)

func OtherHandler(line Entry) {

	domain := line.Field(Domain)
	tag := "OK"

	if line.Field(Domain) != GameloftDomain {
		return
	}
	if strings.Contains(line.Field(request), "subscription_index") == false {
		return
	}
	if line.Field(TS) != "TS=001DEFAULT_TS_WTCOM" {
		return
	}
	if line.Field(Cookie) == "LKU-COOKIE" {
		return
	}
	if line.Field(clientIP)[:3] != "10." {
		return
	}

	IncrementQualifiedEventCounter(domain, tag)

	return
}
