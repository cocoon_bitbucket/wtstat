package source

import (
	"context"
	"fmt"
	"log"
	"net"
	"testing"
	"time"
)

func TestUdpSource(t *testing.T) {

	addr := "127.0.0.1:5600"

	// server
	ctx, cancel := context.WithCancel(context.Background())

	dispatch := &ScreenDispatcher{}

	go UdpServer(ctx, addr, dispatch)

	// Client
	conn, err := net.Dial("udp", addr)
	if err != nil {
		panic(err)
	}
	defer conn.Close()

	// Call the `Write()` method of the implementor
	// of the `io.Writer` interface.
	_, err = fmt.Fprintf(conn, "something")
	if err != nil {
		log.Fatalf("%s\n", err)
	}
	_, _ = fmt.Fprintf(conn, " and something else\n")
	time.Sleep(2 * time.Second)
	print("canceling...")
	cancel()

	time.Sleep(2 * time.Second)

}
