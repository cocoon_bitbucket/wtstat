package source

import (
	"io"
	"log"
	"os"
)

func NewPipeSource(namedPipe string) (pipe io.Reader, err error) {

	pipe, err = os.OpenFile(namedPipe, os.O_RDONLY, os.ModeNamedPipe)
	if err != nil {
		log.Printf("Couldn't open the pipe %s : %s\n", namedPipe, err.Error())
		return
	}
	return
}
