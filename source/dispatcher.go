package source

import (
	"fmt"
)

type DispatcherEngine interface {
	Write(string, []byte) (int, error)
}

//type DispatcheeEngine interface {
//
//	io.Writer 		// write to the dispatchee
//
//}
//
//// a factory for dispatchee
//type DispatcheeFactory func () DispatcheeEngine

//
// screen dispatcher

type ScreenDispatcher struct{}

// implements DispatcherEngine
func (s *ScreenDispatcher) Write(tag string, data []byte) (n int, err error) {
	fmt.Printf("dispatch tag:%s, data:\n%s\n", tag, data)
	return len(data), nil
}

//func NewDispatcherDevice( dispatcheeFactory DispatcheeFactory ) (m *DispatcherDevice, sink io.Writer) {
//	m = &DispatcherDevice{
//		Ctx:              context.Background(),
//		dispatcheeFactory: dispatcheeFactory,
//		Map:              make(map[string]DispatcheeEngine ),
//
//	}
//	return
//}
//
//// a map of dispatchee
//type DispatcherDevice struct {
//	Ctx context.Context
//
//	dispatcheeFactory DispatcheeFactory
//	mux      sync.Mutex
//	Map      map[string]DispatcheeEngine
//
//}
//
//// implements io.Writer
//func ( d DispatcherDevice)  Write( tag string,  data []byte) (n int, err error) {
//	//l.channel <- p
//	//return len(p),nil
//	if _,ok := d.Map[tag] ; ok == false {
//		// target does not exists
//		d.Map[tag] = d.dispatcheeFactory()
//	}
//	dispatchee := d.Map[tag]
//	n,err = dispatchee.Write(data)
//	if err != nil {
//		log.Printf("%s\n",err)
//	}
//	return
//
//}
//

//
//
//
