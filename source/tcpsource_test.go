package source

import (
	"context"
	"fmt"
	"net"
	"testing"
	"time"
)

func TestTcpServer(t *testing.T) {

	addr := "127.0.0.1:8600"

	// server
	ctx, cancel := context.WithCancel(context.Background())

	//collector := os.Stdout
	dispatch := &ScreenDispatcher{}

	go TcpServer(ctx, addr, dispatch)
	time.Sleep(10 * time.Millisecond)

	c1, err := net.Dial("tcp", addr)
	if err != nil {
		fmt.Println(err)
		return
	}
	c2, err := net.Dial("tcp", addr)
	if err != nil {
		fmt.Println(err)
		return
	}
	time.Sleep(10 * time.Millisecond)

	c1.Write([]byte("c1: This is the lines\nc1: and more"))
	c2.Write([]byte("c2: This is the lines\n"))
	c1.Write([]byte("... \n"))

	time.Sleep(5 * time.Second)
	cancel()
	time.Sleep(1 * time.Second)

}
