package source

import (
	"bufio"
	"fmt"
	"log"
	"testing"
)

func TestCollector(t *testing.T) {

	c := NewCollectorPoint(2048)

	n, err := c.Write([]byte("This is one"))
	n, err = c.Write([]byte(" line\n"))

	b := make([]byte, 1024)

	n, err = c.Read(b)
	if err != nil {
		log.Fatal(err.Error())
	}
	read := b[:n]
	fmt.Printf("n:%d, data: %s\n", n, string(read))
	if string(read) != "This is one" {
		t.Fail()
		return
	}

	n, err = c.Read(b)
	if err != nil {
		log.Fatal(err.Error())
	}
	read = b[:n]
	fmt.Printf("n:%d, data: %s\n", n, read)
	if string(read) != " line\n" {
		t.Fail()
		return
	}

}

func TestCollectorBufio(t *testing.T) {

	c := NewCollectorPoint(2048)

	n, err := c.Write([]byte("This is one"))
	n, err = c.Write([]byte(" line\n"))

	_ = n
	_ = err
	var delim byte = '\n'
	buf := bufio.NewReader(c)

	line, err := buf.ReadBytes(delim)
	if err != nil {
		t.Fail()
		return
	}
	if string(line) != "This is one line\n" {
		t.Fail()
		return
	}

}
