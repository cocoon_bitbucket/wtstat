package source

import (
	"context"
	"sync"
	"time"

	//"bufio"
	//"golang.org/x/text/language"
	"io"
	"log"
)

type LineSplitter struct {
	sink io.Writer

	channel chan []byte
	//source io.Reader
}

func NewLineSplitter(sink io.Writer) LineSplitter {
	return LineSplitter{
		sink:    sink,
		channel: make(chan []byte, 1096),
	}
}

//func MakeLineSplitterFactory(sink io.Writer) func() LineSplitter  {
//
//	 x := func() LineSplitter{
//		return NewLineSplitter(sink)
//		}
//		return x
//}

// implements io.Writer
func (l LineSplitter) Write(p []byte) (n int, err error) {
	l.channel <- p
	return len(p), nil
}

// other functions
func (l LineSplitter) Readline() (line []byte, err error) {

	for c := range l.channel {
		line = append(line, c...)
	}
	return line, err
}

// read for source and write delimited chunks to sink
// LineSplitter
func (l LineSplitter) Run() {

	//ch := make(chan string, 1024 )
	//ch := l.channel

	var lastNl int = -1
	var buffer []byte

	// read the channel
	for chunk := range l.channel {

		for i, c := range chunk {
			if c == '\n' {
				lastNl = i
			}
		}
		// end of chunk
		if lastNl == -1 {
			// no nl in chunk : add it to buffer
			buffer = append(buffer, chunk...)
			// read next from channel
			continue
		} else {
			// we got at least a nl :=> send it and store remainder
			keep := chunk[:lastNl+1]
			remainder := chunk[lastNl+1:]

			data := append(buffer, keep...)
			buffer = remainder
			l.send(data)
			lastNl = -1
		}
	}

}

func (l LineSplitter) send(data []byte) {

	n, err := l.sink.Write(data)
	if err != nil {
		// fail to write
		log.Fatalf("fail to write line:%s\n", err)
	}
	if n != len(data) {
		// not all line copied
		log.Fatalf("fail to write all line:%s\n", err)
	}
}

// read for source and write delimited chunks to sink
// LineSplitter
func (l LineSplitter) Run2() {

	//ch := make(chan string, 1024 )
	//ch := l.channel

	var buffer []byte

	// read the channel
	for chunk := range l.channel {

		var lastNl int = -1

		// scan chunk for \n backwards
		for j := len(chunk) - 1; j >= 0; j-- {
			if chunk[j] == '\n' {
				lastNl = j
				break
			}
		}
		// end of chunk
		if lastNl == -1 {
			// no nl in chunk : add it to buffer
			buffer = append(buffer, chunk...)
			// read next from channel
			continue
		} else {
			// we got at least a nl :=> send it and store remainder
			keep := chunk[:lastNl+1]
			remainder := chunk[lastNl+1:]

			data := append(buffer, keep...)
			buffer = remainder
			l.send(data)
			lastNl = -1
		}
	}

}

//
//
//
func NewLineSplitterDispatcherDevice(sink io.Writer) (d *LineSplitterDispatcherDevice) {
	d = &LineSplitterDispatcherDevice{
		Ctx:  context.Background(),
		sink: sink,
		//Map:              make(map[string]DispatcheeEngine ),
		Map: make(map[string]LineSplitter),
	}
	return
}

// a map of dispatchee
type LineSplitterDispatcherDevice struct {
	Ctx context.Context

	sink io.Writer
	mux  sync.Mutex
	//Map      map[string]DispatcheeEngine
	Map map[string]LineSplitter
}

// implements io.Writer
func (d LineSplitterDispatcherDevice) Write(tag string, data []byte) (n int, err error) {

	var dispatchee LineSplitter
	d.mux.Lock()
	if _, ok := d.Map[tag]; ok == false {
		// target dispatchee  does not exists: create it
		dispatchee = NewLineSplitter(d.sink)
		go dispatchee.Run2()
		d.Map[tag] = dispatchee
		time.Sleep(1 * time.Millisecond)
	} else {
		dispatchee = d.Map[tag]
	}
	d.mux.Unlock()
	n, err = dispatchee.Write(data)
	if err != nil {
		log.Printf("%s\n", err)
	}
	return

}
