package source

import "io"

//
// collector
//

type Collector interface {
	io.Reader
	io.Writer
}

// multiple write source sink read source
// Collector
type CollectorDevice struct {
	//mutex sync.Mutex
	channel chan []byte
}

// write to pipe
func (w *CollectorDevice) Write(p []byte) (n int, err error) {
	w.channel <- p
	return len(p), nil
}

// read from pipe
func (w *CollectorDevice) Read(p []byte) (n int, err error) {

	b := <-w.channel
	for i, c := range b {
		//p = append(p,c)
		p[i] = c
	}
	return len(b), nil
}

func NewCollectorPoint(size int) (ch *CollectorDevice) {

	return &CollectorDevice{
		channel: make(chan []byte, size),
	}

}
