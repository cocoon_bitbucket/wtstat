package source

import (
	"io"
	"log"
	"os"
)

func NewFileSource(filename string) (file io.Reader, err error) {

	file, err = os.Open(filename)
	if err != nil {
		log.Printf("Couldn't open the file %s : %s\n", filename, err.Error())
	}
	return
}
