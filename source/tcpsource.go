package source

import (
	"context"
	"fmt"
	"log"
	"math/rand"
	"net"
	"time"
)

func TcpServer(ctx context.Context, address string, dispatch DispatcherEngine) (err error) {

	// address is like "0.0.0.0:8080
	//PORT := ":" + arguments[1]
	l, err := net.Listen("tcp4", address)
	if err != nil {
		fmt.Println(err)
		return
	}
	defer l.Close()
	rand.Seed(time.Now().Unix())

	for {
		c, err := l.Accept()
		if err != nil {
			fmt.Println(err)
			return err
		}
		go handleConnection(c, dispatch)
	}

}

func handleConnection(conn net.Conn, dispatch DispatcherEngine) {
	fmt.Printf("Serving %s\n", conn.RemoteAddr().String())
	buf := make([]byte, 1096)
	for {

		//netData, err := bufio.NewReader(conn).ReadBytes('\n')
		//netData, err := bufio.NewReader(c).ReadString('\n')
		//var data []byte
		n, err := conn.Read(buf)
		if err != nil {
			fmt.Println(err)
			continue
		}
		if n == 0 {
			time.Sleep(10 * time.Millisecond)
			continue
		}

		_, err = dispatch.Write(conn.RemoteAddr().String(), buf[:n])
		if err != nil {
			log.Printf("%s\n", err.Error())
		}

	}
	conn.Close()
}
