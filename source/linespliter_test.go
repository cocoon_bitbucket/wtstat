package source

import (
	"os"
	"strings"
	"testing"
	"time"
)

func TestLineSplitter(t *testing.T) {

	lines := []byte("This is 2 lines")
	lines2 := []byte("\nthe second one\n")

	var sink = os.Stdout

	s1 := NewLineSplitter(sink)
	s1.Write(lines)
	s1.Write(lines2)

	go s1.Run2()
	time.Sleep(1 * time.Second)

	//var p[]byte
	time.Sleep(5 * time.Second)

}

func TestLineSplitterDispatcher(t *testing.T) {

	var dataflow = []string{
		"s1: This is 2 lines ",
		"s2: This is 2 lines\n",
		"s1: \nthe second one\n",
		"s2: This is second line\n",
	}

	//lines := []byte("This is 2 lines")
	//lines2 := []byte("\nthe second one\n")

	var sink = os.Stdout

	s := NewLineSplitterDispatcherDevice(sink)

	for _, line := range dataflow {
		parts := strings.Split(line, ":")
		s.Write(parts[0], []byte(line))
	}

	//s.Write("s1", lines)
	//s.Write("s1", lines2)

	time.Sleep(1 * time.Second)

	//var p[]byte
	time.Sleep(5 * time.Second)

}
