package wtstat

import (
	//"bitbucket.com/cocoon_bitbucket/wtstat/datasource"
	//"errors"
	//"fmt"
	"log"
	//"strconv"
	//"strings"
	"time"
)

const (
	SCANNER_TYPE = "wtstat"
	SCANNER_NAME = "1"

	SCANNER_TIME_FORMAT = "02/01/2006:15:04:05"
)

type ScannerConfig struct {
	Type  string // wtstat
	Name  string // 1
	Debug bool   //

	//Source datasource.DataSource

	CsvSource Source
	Mode      string // FILE or PIPE

	ReplayMode    string    // "": preserve trace delay  ,  "0" : nodelay , "100ms" : 100ms delay
	LastTimestamp time.Time // remember last timeStamp
}

//func NewWtstatLog(line string) (w *WtstatLog, err error) {
//
//	// remove last \n
//	line = line[:len(line)-1]
//
//	f := strings.Split(line, "|")
//	n := len(f)
//	if len(f) == 0 {
//		err = errors.New("bad format")
//		return
//	}
//
//	w = &WtstatLog{
//		Fields: f,
//		Size:   n,
//	}
//	return
//}

func Scanner(cnf *ScannerConfig) {

	source := cnf.CsvSource

	done := false
	for i := 1; done == false; i++ {

		entry, err := source.Read()
		if err != nil {
			if err.Error() == "EOF" {
				break
			}
			log.Print(err)
			IncrementEventCounter("1", "NA")
			continue
		}

		if cnf.Debug == true {
			log.Printf("%v\n", entry)
		}
		if cnf.ReplayMode != "0" {
			err = HandleReplayMode(entry, cnf)
			if err != nil {
				// fail to handle timestamp
				IncrementEventCounter("1", "NA")
				continue
			}
		}

		Dispatch(entry)

	}
}

// HandleReplaymode generate a delay if requested ( fixed delay or respect trace timestamp delay)
func HandleReplayMode(line Entry, cnf *ScannerConfig) (err error) {

	// handle replay mode
	var delay time.Duration
	if cnf.ReplayMode != "0" {
		// apply replay mode
		ts, err := line.Timestamp()
		if err != nil {
			//IncrementEventCounter("1", "NA")
			return err
		}
		if (time.Time{}) != cnf.LastTimestamp {
			// we got a last timestamp
			if cnf.ReplayMode == "" {
				// replay mode respect source delay
				dif := ts.Sub(cnf.LastTimestamp)
				if dif > 0 {
					delay = dif
				}
			} else {
				// replay mode is fix delay
				delay, err = time.ParseDuration(cnf.ReplayMode)
				if err != nil {
					log.Printf("")
					delay, _ = time.ParseDuration("1s")
				}
			}

		} else {
			// empty time.Time , first element : no delay
			cnf.LastTimestamp = ts
			delay = 0
		}

	} else {
		// we dont handle replay mode
		delay = 0
	}

	if delay > 0 {
		// generate the delay
		if cnf.Debug == true {
			log.Printf("wait for :%s\n", delay.String())
		}
		time.Sleep(delay)
	}

	return nil
}
