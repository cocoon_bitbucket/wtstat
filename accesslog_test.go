package wtstat

import (
	"io"
	"log"
	"os"
	"testing"
)

func TestAcceslogObject(t *testing.T) {

	filename := "./samples/access_log"

	// Open the file
	csvfile, err := os.Open(filename)
	if err != nil {
		log.Fatalln("Couldn't open the csv file", err)
	}

	l, err := NewAccesLog("file_acces_log", csvfile)
	if err != nil {
		log.Fatalf(err.Error())
	}

	// Iterate through the records
	for {
		// Read each record from csv
		entry, err := l.Read()
		if err == io.EOF {
			break
		}
		if err != nil {
			log.Fatal(err)
		}

		//timestamp := record[TimeStamp]
		//domain := record[Domain]
		//request := record[request]
		//cookie := entry.Field(Cookie)

		cookie := entry[Cookie]
		ts, _ := entry.Timestamp()
		println(ts.String())
		//ts := record[TS]

		//println(timestamp, domain, request, cookie, ts)
		//println(ts)
		println(cookie)

		//fmt.Printf("Question: %s Answer %s\n", record[0], record[1])
	}
}
