package wtstat

import (
	"errors"
	"fmt"
	"strconv"
	"strings"
	"time"
)

/*
	Entry: a csv line




*/

type Entry []string

func (e Entry) Field(n FieldType) string {
	return e[n]
}

// get timestamp from fields and convert to time.Time
//  timestamp field format [05/11/2019:01:12:45]
func (e Entry) Timestamp() (timestamp time.Time, err error) {

	st := e[TimeStamp]
	if len(st) <= 0 {
		err = errors.New("bad timestamp")
		return
	}
	if st[0] != '[' {
		err = errors.New("bad timestamp")
		return
	}
	st = st[1 : len(st)-2]
	// split into date time ( because time is unparsable ( not 2 digit time reprsentation  )
	date := st[:10]
	tm := st[11:]
	//println(date,tm)
	parts := strings.Split(tm, ":")
	if len(parts) != 3 {
		err = errors.New("bad timestamp")
	}
	h, _ := strconv.Atoi(parts[0])
	m, _ := strconv.Atoi(parts[1])
	s, _ := strconv.Atoi(parts[2])

	// form a correct timestamp string to be parsable
	st = date + fmt.Sprintf(":%2.2d:%2.2d:%2.2d", h, m, s)

	//st = "04/11/2019:01:14:_2"
	timestamp, err = time.Parse(SCANNER_TIME_FORMAT, st)
	if err != nil {
		err = errors.New("bad timestamp")
		return
	}
	return
}

type Source interface {
	Read() (Entry, error)
}
