package wtstat

import (
	"bitbucket.com/cocoon_bitbucket/wtstat/datasource"
	"log"
	"testing"
	"time"
)

func TestScannerFileMode(t *testing.T) {

	// init prometheus metrics
	InitMetrics()

	// create data source
	source, err := datasource.NewFileSource("./samples/access_log", "")
	if err != nil {
		log.Fatal(err)
	}

	accesslog, err := NewAccesLog("file_acces_log", source)
	if err != nil {
		log.Fatal(err)
	}

	cnf := &ScannerConfig{
		Type:          "acceslog",
		Name:          "1",
		Debug:         true,
		CsvSource:     accesslog,
		Mode:          "",
		ReplayMode:    "0",
		LastTimestamp: time.Time{},
	}

	// launch scanner
	Scanner(cnf)

}

func TestScannerWithServer(t *testing.T) {

	httpPort := "8080"

	// init prometheus metrics
	InitMetrics()

	// start metrics server
	go Server(httpPort)

	source, err := datasource.NewFileSource("./samples/access_log", "")
	if err != nil {
		log.Fatal(err)
	}

	accesslog, err := NewAccesLog("file_acces_log", source)
	if err != nil {
		log.Fatal(err)
	}
	// launch scanner
	cnf := &ScannerConfig{
		Type:          "accesslog",
		Name:          "1",
		Debug:         true,
		CsvSource:     accesslog,
		Mode:          "",
		ReplayMode:    "",
		LastTimestamp: time.Time{},
	}
	go Scanner(cnf)

	// wait a while
	tm := time.NewTimer(120 * time.Second)
	select {
	case <-tm.C:
		// timeout
		break
	}

}
