package wtstat

import "strings"

/*

	gameloft filter

	condition 1 => champ « Nom de domaine » contient gameloft.orange.sn
  	condition 2 => champ « Requete » contient /subscription_index/
  	condition 3 => champ « TS » contient TS=001DEFAULT_TS_WTCOM
  	condition 4 => champ « TS » ne contient pas TS=001DEFAULT_TS_WTCOM_NA
  	condition 5 => champ « Cookie » ne contient pas LKU-COOKIE
  	condition 6 => champ « Client-IP » contient 10.xx.xx.xx


*/

const (
	GameloftDomain = "gameloft.orange.sn"
)

func GameloftHandler(line Entry) {

	domain := line.Field(Domain)
	tag := "OK"

	if line.Field(Domain) != GameloftDomain {
		return
	}
	if strings.Contains(line.Field(request), "subscription_index") == false {
		return
	}
	if line.Field(TS) != "TS=001DEFAULT_TS_WTCOM" {
		return
	}
	if line.Field(Cookie) == "LKU-COOKIE" {
		return
	}
	if line.Field(clientIP)[:3] != "10." {
		return
	}

	IncrementQualifiedEventCounter(domain, tag)

	return
}
