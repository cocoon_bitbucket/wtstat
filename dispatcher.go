package wtstat

func Dispatch(line Entry) {

	domain := line.Field(Domain)
	IncrementEventCounter("", domain)

	switch domain {

	case GameloftDomain:
		GameloftHandler(line)

	case RcsDomain:
		RcsHandler(line)

	case MyorangeDomain:
		MyorangeHandler(line)

	case WaaatDomain:
		WaaatHandler(line)

	case OmapposnDomain:
		OmapposnHandler(line)

	}
}
