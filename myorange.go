package wtstat

import (
	"github.com/prometheus/client_golang/prometheus"
	"regexp"
	"strings"
)

// import "strings"

const MyorangeDomain = "myorange.orange.sn"

var (
	vipWaaat = "172.25.9.7"

	reWtcom            = regexp.MustCompile(`TS=001DEFAULT_TS_WTCOM`)
	reWtcomNa          = regexp.MustCompile(`TS=001DEFAULT_TS_WTCOM_NA`)
	reAccountConsulted = regexp.MustCompile(
		`(subsection\=account\&subsection=balance|subsection\=balance\&subsection\=account)`)
)

func IncrementEdenRequestCounted(domain string) {
	EdenRequestCounted.With(
		prometheus.Labels{
			"domain": domain,
		}).Inc()
}
func IncrementEdenRequestOk(domain string) {
	EdenRequestOk.With(
		prometheus.Labels{
			"domain": domain,
		}).Inc()
}
func IncrementEdenOkAccountConsulted(domain string) {
	EdenOkAccountConsulted.With(
		prometheus.Labels{
			"domain": domain,
		}).Inc()
}

func MyorangeHandler(line Entry) {

	tag := ""

	rq := line[request]
	_ = rq
	domain := line.Field(Domain)
	//
	// the filter instructions start below
	//

	// request uri contains /ExposeV1/
	if strings.Contains(line.Field(request), "ExposeV1") == true {

		// request is GET
		if strings.HasPrefix(line.Field(request), "GET") {

			// ts := ^001DEFAULT_TS_WTCOM or ts :=  ^001DEFAULT_TS_WTCOM_NA
			ts := line[TS]

			// TS is TS=001DEFAULT_TS_WTCOM or TS=001DEFAULT_TS_WTCOM_NA
			if reWtcom.MatchString(ts) || reWtcomNa.MatchString(ts) {
				// eden_request_counted ++
				IncrementEdenRequestCounted(domain)
			}

			if reWtcom.MatchString(ts) && strings.Contains(line.Field(HttpCode), "200") {
				//
				if strings.Contains(line.Field(request), "myorangewebapp") &&
					strings.Contains(line.Field(Cookie), "LKU-COOKIE") {
					// NO OP
				} else {
					IncrementEdenRequestOk(domain)

					// subsection account and balance
					if reAccountConsulted.MatchString(line.Field(request)) {
						IncrementEdenOkAccountConsulted(domain)
					}

					// nat fake cookie
					if strings.Contains(line.Field(nat), "nat=1") &&
						strings.Contains(line.Field(Cookie), "FAKE-COOKIE") {

						// remoteip == client ip
						if line.Field(clientIP) == line.Field(remoteIP) {
							if line.Field(remoteIP) != vipWaaat {
								match, _ := regexp.MatchString("^10.", line.Field(clientIP))
								if !match {
									// proxy net1 fraud
								}
							}
						}

					}
				}

			}
		}
	}

	//
	// create the prometheus metric
	//
	IncrementQualifiedEventCounter(line.Field(Domain), tag)

	return
}
