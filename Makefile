#include .env

PROJECTNAME=$(shell basename "$(PWD)")

GOBASE=$(shell pwd)
#GOPATH=$(GOBASE)


# Make is verbose in Linux. Make it silent.
#MAKEFLAGS += --silent

hello :
	echo "Hello ${PROJECTNAME} at ${GOBASE}, GOPATH=${GOPATH}";

tests:
	go test

compile:
	echo "Compiling for every OSX and linux Platforms"
	GOOS=darwin GOARCH=amd64 go build -o build/packages/darwin_amd64/wtstat ./cmd/wtstat/wtstat.go
	GOOS=linux GOARCH=amd64 go build -o build/packages/linux_amd64/wtstat ./cmd/wtstat/wtstat.go
	cp build/packages/linux_amd64/wtstat build/docker/demo/wtstat/src/wtstat
